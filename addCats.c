///////////////////////////////////////////////////////////////////////////////
//////          University of Hawaii, College of Engineering
/////// @brief  Lab 05d - AnimaL Farm0 - EE 205 - Spr 2022
//////
////// Usage:  Animal Farm0
//////
//////
//////
//////
//////
////// @author Carl Domingo <carld20@hawaii.edu>
////// @date   02/19/2022
///////////////////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdbool.h>
#include "addCats.h"
#include "catDatabase.h"

int addCat ( enum char* newName, enum Gender gender, enum Breeds breeds, newFixed, newWeight ) {
	while ( numCats != MAX_CATS ){
		if ( strlen(newName) != 0 || strlen(newName) < MAXCHAR){
			name[i] = newName;
		}
		else{
			return 1;
		}

		gender[i]  = gender;
		breeds[i] = breed;
		fixed[i] = newFixed;
		if (newWeight > 0 ){
			weight[i] = newWeight;
		}
		else{
			return 1;
		}
		
		index = index + 1;
	}
	return index;

}
