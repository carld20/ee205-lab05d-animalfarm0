///////////////////////////////////////////////////////////////////////////////
//////          University of Hawaii, College of Engineering
/////// @brief  Lab 05d - AnimaL Farm0 - EE 205 - Spr 2022
//////
////// Usage:  Animal Farm0
//////
//////
//////
//////
//////
////// @author Carl Domingo <carld20@hawaii.edu>
////// @date   02/19/2022
///////////////////////////////////////////////////////////////////////////////////

#define MAX_CATS (30)
#define MAXCHAR (30)

int index = 0;

char name[MAX_CATS];
int numCats = index + 1;


enum char Gender { Male, Female, Unknown };
char Gender [MAX_CATS];
extern enunm Gender gender;


enum char Breeds { UKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX };
char Breeds [MAX_CATS];
extern enum Breeds breeds;

bool isFixed;
isFixed = FALSE;
bool isFixed [MAX_CATS];
extern enum isFixed fixed;

float Weight;
Weight[MAX_CATS];
extern Weight weight;



