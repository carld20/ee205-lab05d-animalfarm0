///////////////////////////////////////////////////////////////////////////////
//////          University of Hawaii, College of Engineering
/////// @brief  Lab 05d - AnimaL Farm0 - EE 205 - Spr 2022
//////
////// Usage:  Animal Farm0
//////
//////
//////
//////
//////
////// @author Carl Domingo <carld20@hawaii.edu>
////// @date   02/19/2022
///////////////////////////////////////////////////////////////////////////////////

#include "deleteCats.h"
#include "addCats.h"


deleteAllCats(){
	for (int k = 0; k < numCats; k++ ){
		name[k] = " ";
		Gender[k] = " ";
		breeds[k] = " ";
		isFixed[k] = 0;
		weight[k] = 0;
	}
