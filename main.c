///////////////////////////////////////////////////////////////////////////////
////          University of Hawaii, College of Engineering
///// @brief  Lab 05d - AnimaL Farm0 - EE 205 - Spr 2022
////
//// Usage:  Animal Farm0
////
////
////
////
////
//// @author Carl Domingo <carld20@hawaii.edu>
//// @date   02/19/2022
/////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "catDatabase.h"
#include "reportCats.h"
#include "deleteCats.h"
#include "addCats.h"
#include "updateCats.h"


int main() {
	printf("Starting Animal Farm");
	
	addCat ("Dominick", Female, MAINCE_COON,   FALSE, 15.0 ) ;
	addCat ("Seabass",  Uknown, UNKNOWN_BREED, TRUE,   2.3 ) ;
	addCat ("Kebin",    Female, MANX,          FALSE, 25.9 ) ;
	addCat ("Brae",     Female, PERSIAN,       FALSE, 19.7 ) ;
	addCat ("Izaiah",   Uknown, SHORTHAIR,     TRUE,  10.0 ) ;
	addCat ("Kurris",   Male,   UNKOWN_BREED,  TRUE,  13.0 ) ;
	addCat ("Jacob",    Female, SHORTHAIR,     FALSE, 22.0 ) ;
	addCat ("Alden",    Female, MAINE_COON,    FALSE, 35.8 ) ;
	addCat ("Jeriel",   Uknown, Uknown_Breed,  TRUE,  28.3 ) ;
	addCat ("Dan Dan",  Uknown, Manx,          TRUE,   2.0 ) ;
	addCat ("Paul",     Female, UNKNOWN_BREED, FALSE,  0.2 ) ;

	printAllCats();

	fixCat(2);
	updateCatName( 1, "Dom");

	printAllCats();

	deleteAllCats();
	printAllCats();
	

	return 0;

}
