///////////////////////////////////////////////////////////////////////////////
//////          University of Hawaii, College of Engineering
/////// @brief  Lab 05d - AnimaL Farm0 - EE 205 - Spr 2022
//////
////// Usage:  Animal Farm0
//////
//////
//////
//////
//////
////// @author Carl Domingo <carld20@hawaii.edu>
////// @date   02/19/2022
///////////////////////////////////////////////////////////////////////////////////

#include "updateCats.h"
#include "catDataBase.h"

char updateCatName ( i, char newName ){

	return name[i] = newName;
	
}

bool fixCat(i){
	if (bool[i] != TRUE){
		return bool[i] = TRUE;
	}
}

float updateCatWeight( i, float newWeight ){
	if (newWeight < 0){
		return 1;
	}
	else{
		return weight[i] = newWeight;
	}
}

