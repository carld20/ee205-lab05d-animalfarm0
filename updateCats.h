///////////////////////////////////////////////////////////////////////////////
//////          University of Hawaii, College of Engineering
/////// @brief  Lab 05d - AnimaL Farm0 - EE 205 - Spr 2022
//////
////// Usage:  Animal Farm0
//////
//////
//////
//////
//////
////// @author Carl Domingo <carld20@hawaii.edu>
////// @date   02/19/2022
///////////////////////////////////////////////////////////////////////////////////

int i = i - 1;


extern char updateCatName( i, char newName );

extern bool fixCat(i);

extern float updateCatWeight( i, float newWeight );

